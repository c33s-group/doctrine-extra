<?php

declare(strict_types=1);

namespace Tests\Classes\NanoIdGeneratorTest;

use C33s\Doctrine\ORM\Id\RespectfulNanoIdGenerator;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class NanoIdTestEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="nanoid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=RespectfulNanoIdGenerator::class)
     */
    public $id;

    /**
     * @ORM\Column(type="string", unique=true, length=60)
     */
    public $name;

    /**
     * Temp constructor.
     *
     * @param $id
     */
    public function __construct($id = null)
    {
        $this->id = $id;
    }
}
