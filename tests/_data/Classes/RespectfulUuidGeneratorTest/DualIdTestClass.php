<?php

declare(strict_types=1);

namespace Tests\Classes\RespectfulUuidGeneratorTest;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class DualIdTestClass
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="C33s\Doctrine\ORM\Id\RespectfulUuidGenerator")
     */
    public $id;
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     */
    public $subId;

    /**
     * Temp constructor.
     *
     * @param $id
     * @param $subId
     */
    public function __construct($id = null, $subId = null)
    {
        $this->id = $id;
        $this->subId = $subId;
    }
}
