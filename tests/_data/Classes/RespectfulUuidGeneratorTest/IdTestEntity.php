<?php

declare(strict_types=1);

namespace Tests\Classes\RespectfulUuidGeneratorTest;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class IdTestEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="C33s\Doctrine\ORM\Id\RespectfulUuidGenerator")
     */
    public $id;

    /**
     * Temp constructor.
     *
     * @param $id
     */
    public function __construct($id = null)
    {
        $this->id = $id;
    }
}
