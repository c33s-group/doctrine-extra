<?php

declare(strict_types=1);

namespace Tests\Classes\JsonValueObjectTest\Value;

final class City
{
    private $name;

    public static function fromString(string $name): self
    {
        return new self($name);
    }

    private function __construct(string $name)
    {
        $this->name = $name;
    }

    public function name(): string
    {
        return $this->name;
    }
}
