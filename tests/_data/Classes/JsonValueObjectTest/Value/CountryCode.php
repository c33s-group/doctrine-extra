<?php

declare(strict_types=1);

namespace Tests\Classes\JsonValueObjectTest\Value;

use Webmozart\Assert\Assert;

final class CountryCode
{
    private $code;

    public static function fromString(string $code): self
    {
        return new self($code);
    }

    private function __construct(string $code)
    {
        Assert::length($code, 2);
        $this->code = $code;
    }

    public function code(): string
    {
        return $this->code;
    }
}
