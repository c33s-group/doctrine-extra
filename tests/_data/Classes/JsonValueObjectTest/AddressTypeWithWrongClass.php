<?php

declare(strict_types=1);

namespace Tests\Classes\JsonValueObjectTest;

use C33s\Doctrine\Types\AbstractValueObjectJsonType;

class AddressTypeWithWrongClass extends AbstractValueObjectJsonType
{
    protected $name = 'test_address';
    protected $valueObjectClass = AddressValueObjectNotImplementingArrayInterface::class;
}
