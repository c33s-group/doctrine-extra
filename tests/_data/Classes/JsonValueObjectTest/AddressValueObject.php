<?php

declare(strict_types=1);

namespace Tests\Classes\JsonValueObjectTest;

use C33s\Doctrine\Interfaces\ArrayInterface;
use Tests\Classes\JsonValueObjectTest\Value\AddressLine;
use Tests\Classes\JsonValueObjectTest\Value\City;
use Tests\Classes\JsonValueObjectTest\Value\CountryCode;
use Tests\Classes\JsonValueObjectTest\Value\Province;
use Tests\Classes\JsonValueObjectTest\Value\Zip;
use Webmozart\Assert\Assert;

final class AddressValueObject implements ArrayInterface
{
    /**
     * @var AddressLine
     */
    private $streetLine1;

    /**
     * @var AddressLine
     */
    private $streetLine2;

    /**
     * @var City
     */
    private $city;

    /**
     * @var Zip
     */
    private $zip;

    /**
     * @var Province
     */
    private $province;

    /**
     * @var CountryCode
     */
    private $country;

    public static function fromObjects(
        AddressLine $streetLine1,
        AddressLine $streetLine2,
        City $city,
        Zip $zip,
        Province $province,
        CountryCode $country
    ): self {
        return new self(
            $streetLine1,
            $streetLine2,
            $city,
            $zip,
            $province,
            $country
        );
    }

    /**
     * Create an address from one string. Must contain all elements separated by newlines.
     *
     * <code>
     *  Street1
     *  Street2
     *  City
     *  Zip
     *  Province
     *  Country
     * </code>
     *
     * @param mixed $fullAddress
     */
    public static function fromString($fullAddress)
    {
        $addressElements = str_replace(["\r\n", "\r"], "\n", $fullAddress);
        $addressElementsArray = explode("\n", $addressElements);

        Assert::isArray($addressElementsArray);
        Assert::count($addressElementsArray, 6);

        return self::fromStrings(...$addressElementsArray);
    }

    public static function fromStrings(
        string $streetLine1,
        string $streetLine2,
        string $city,
        string $zip,
        string $province,
        string $country
    ) {
        return new self(
            AddressLine::fromString($streetLine1),
            AddressLine::fromString($streetLine2),
            City::fromString($city),
            Zip::fromString($zip),
            Province::fromString($province),
            CountryCode::fromString($country)
        );
    }

    public static function fromArray(array $array)
    {
        return self::fromStrings(
            $array['line1'],
            $array['line2'],
            $array['city'],
            $array['zip'],
            $array['province'],
            $array['country_code']
        );
    }

    public function toArray(): array
    {
        return [
            'line1' => $this->streetLine1->content(),
            'line2' => $this->streetLine2->content(),
            'city' => $this->city->name(),
            'zip' => $this->zip->code(),
            'province' => $this->province->name(),
            'country_code' => $this->country->code(),
        ];
    }

    private function __construct(
        AddressLine $streetLine1,
        AddressLine $streetLine2,
        City $city,
        Zip $zip,
        Province $province,
        CountryCode $country
    ) {
        $this->streetLine1 = $streetLine1;
        $this->streetLine2 = $streetLine2;
        $this->city = $city;
        $this->zip = $zip;
        $this->province = $province;
        $this->country = $country;
    }
}
