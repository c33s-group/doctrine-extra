<?php

declare(strict_types=1);

namespace Tests\Classes\StringValueObjectTest;

use C33s\Doctrine\Interfaces\StringInterface;

class EmailValueObject implements StringInterface
{
    private $value;

    public static function fromString(string $email)
    {
        return new static($email);
    }

    final public function __construct(string $email)
    {
        $this->value = $email;
    }

    public function __toString()
    {
        return $this->value;
    }

    public function value(): string
    {
        return $this->value;
    }

    public function toString(): string
    {
        return $this->value;
    }
}
