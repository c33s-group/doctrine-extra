<?php

declare(strict_types=1);

namespace Tests\Classes\StringValueObjectTest;

use C33s\Doctrine\Types\AbstractValueObjectStringType;

class EmailType extends AbstractValueObjectStringType
{
    protected $name = 'test_email';
    protected $valueObjectClass = EmailValueObject::class;
}
