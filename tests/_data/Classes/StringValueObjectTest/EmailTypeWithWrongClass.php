<?php

declare(strict_types=1);

namespace Tests\Classes\StringValueObjectTest;

use C33s\Doctrine\Types\AbstractValueObjectStringType;

class EmailTypeWithWrongClass extends AbstractValueObjectStringType
{
    protected $name = 'wrong_email';
    protected $valueObjectClass = EmailValueObjectNotImplementingStringInterface::class;
}
