<?php

declare(strict_types=1);

namespace Test\Functional;

use FunctionalTester;

/**
 * @group admin
 */
class AdminCest
{
//    public function _before(FunctionalTester $I)
//    {
//        $I->loggedInAs('admin', 'main', ['ROLE_ADMIN']);
//    }
//
//    public function _after(FunctionalTester $I)
//    {
//    }
//
//    public function AllMenuSectionsAccessible(FunctionalTester $I)
//    {
//        $I->amOnPage('/admin/');
//        $I->see('<INPUT TEXT HERE>');
//        $I->seeInSource('<a href="/admin/?entity=EntityName');
//        $I->see('<INPUT TEXT HERE>');
//    }
//
//    /**
//     * @test
//     *
//     * @param FunctionalTester $I
//     * @param \Codeception\Example $example
//     * @dataProvider provideAdminEntityData
//     */
//    public function EntityAdminTest(FunctionalTester $I, \Codeception\Example $example)
//    {
//        $entity = $example[0];
//        $entityFQCN = 'App\\Entity\\' . $entity;
//        $name = $example[1];
//        $formNameField = $example[2];
//        $entityCheckField = $example[3];
//        $I->amOnPage('/admin/?entity=' . $entity . '&action=list');
//        $I->click($name);
//        $I->seeResponseCodeIsSuccessful();
////        // check for current items
//        $I->see("${name} erstellen");
//        $I->click("${name} erstellen");
//        $I->seeInSource('<h1 class="title">' . $name . ' erstellen</h1>');
//        $I->seeLink('Zurück zur Übersicht');
//        $I->click('Zurück zur Übersicht');
//        $I->see("${name} erstellen");
//        $I->click("${name} erstellen");
//        $I->seeInSource('<h1 class="title">' . $name . ' erstellen</h1>');
//        $I->fillField($formNameField, 'My New ' . $name);
//        $I->click('Änderungen speichern');
//        $I->seeInSource('<h1 class="title">' . $name . '</h1>');
//        $I->canSeeInRepository($entityFQCN, [$entityCheckField => 'My New ' . $name]);
//        $I->amOnPage('/admin/?entity=' . $entity . '&action=list');
////        $I->reload();
////        $this->reloadPage();
//        $I->seeInSource('My New ' . $name);
//        $I->seeInSource('<a class="text-primary action-edit" title="bearbeiten"');
//        $I->seeInSource('<a class="text-danger action-delete" title="löschen"');
//    }
//
//    public function provideAdminEntityData()
//    {
//        return [
//            ['EntityName', 'Entity Headline', 'entity_name[name]', 'name'],
//        ];
//    }
}
