<?php

declare(strict_types=1);

namespace Tests\Unit\C33s\Doctrine\ORM\Id;

use C33s\Doctrine\ORM\Id\RespectfulNanoIdGenerator;
use C33s\NanoId\NanoId;
use C33s\NanoId\NanoIdFactory;
use Codeception\Test\Unit;
use Tests\Classes\NanoIdGeneratorTest\NanoIdTestEntity;
use Tests\Classes\NanoIdGeneratorTest\NanoIdTestEntityWithLength;
use Tests\Classes\NanoIdGeneratorTest\NanoIdTestEntityWithLengthAndModeAndAlphabet;
use Tests\Unit\Traits\DatabaseIdTrait;

/**
 * @group generator
 * @group nanoid
 */
final class RespectfulNanoIdGeneratorTest extends Unit
{
    public const LONGER_TEST_LENGTH = 10;
    public const MINI_ALPHABET = 'a';

    use DatabaseIdTrait;

    /**
     * @test
     */
    public function idWillBeGeneratedIfNotSet(): void
    {
        $em = $this->getEntityManager();
        $generator = new RespectfulNanoIdGenerator();
        $entity = new NanoIdTestEntity();
        $generatedNanoId = $generator->generate($em, $entity);

        $this->assertInstanceOf(NanoId::class, $generatedNanoId);
        $this->assertEquals(NanoIdFactory::DEFAULT_LENGTH, strlen($generatedNanoId->toString()));
    }

    /**
     * @test
     */
    public function lengthCanBeSetThroughEntity(): void
    {
        $em = $this->getEntityManager();
        $generator = new RespectfulNanoIdGenerator();
        $entity = new NanoIdTestEntityWithLength();
        $generatedNanoId = $generator->generate($em, $entity);

        $this->assertEquals(self::LONGER_TEST_LENGTH, strlen($generatedNanoId->toString()));
    }

    /**
     * @test
     */
    public function alphabetCanBeSetThroughEntity(): void
    {
        $em = $this->getEntityManager();
        $generator = new RespectfulNanoIdGenerator();
        $entity = new NanoIdTestEntityWithLengthAndModeAndAlphabet();
        $generatedNanoId = $generator->generate($em, $entity);

        $this->assertEquals(self::LONGER_TEST_LENGTH, strlen($generatedNanoId->toString()));
        $this->assertEquals(str_repeat(self::MINI_ALPHABET, self::LONGER_TEST_LENGTH), $generatedNanoId->toString());
    }
}
