<?php

declare(strict_types=1);

namespace Tests\Traits;

trait MakeDoctrineTypeObjectTrait
{
    /**
     * Returns example object which extends the AbstractValueObjectStringType.
     * Doctrine types need to be created without calling __construct.
     * Mocking instead.
     *
     * @see https://codeception.com/docs/reference/Mock
     */
    private function getDoctrineTypeObjectFromClassString(string $class)
    {
        return $this->make($class);
    }
}
