<?php

declare(strict_types=1);

namespace C33s\Doctrine\Filter\Interfaces;

interface VisibleInterface
{
    public function isVisible(): bool;
}
