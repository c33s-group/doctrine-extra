<?php

declare(strict_types=1);

namespace C33s\Doctrine\Repository;

use C33s\Doctrine\Repository\Interfaces\StandardEntityRepositoryInterface;
use Doctrine\ORM\EntityRepository as BaseEntityRepository;

class StandardEntityEntityRepository extends BaseEntityRepository implements StandardEntityRepositoryInterface
{
    use Traits\SaveDelete;
}
