<?php

declare(strict_types=1);

namespace C33s\Doctrine\Repository\Traits;

trait DefaultSortByName
{
    /**
     * {@inheritdoc}
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        $orderBy = null === $orderBy ? ['name' => 'asc'] : $orderBy;

        return parent::findBy($criteria, $orderBy, $limit, $offset);
    }
}
