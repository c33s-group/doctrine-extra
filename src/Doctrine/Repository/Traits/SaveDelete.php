<?php

declare(strict_types=1);

namespace C33s\Doctrine\Repository\Traits;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Persistence\Proxy as LegacyProxy;
use Doctrine\Persistence\Proxy;
use InvalidArgumentException;

trait SaveDelete
{
    /**
     * Method which is available in a Doctrine Repository class to get the Class name of the entity.
     *
     * {@inheritdoc}
     */
    abstract public function getClassName();

    /**
     * Allows to save one or more entities from the repository.
     *
     * @param object|Collection|LegacyProxy|LegacyProxy[]|Proxy|Proxy[] $objects
     */
    public function save($objects, bool $doFlush = true): void
    {
        $objects = $this->normalize($objects);

        foreach ($objects as $object) {
            $this->validate($object);
            $this->_em->persist($object);
        }

        if ($doFlush) {
            $this->_em->flush();
        }
    }

    /**
     * Allows to delete one or more entities from the repository.
     *
     * @param object|Collection|LegacyProxy|LegacyProxy[]|Proxy|Proxy[] $objects
     */
    public function delete($objects, bool $doFlush = true): void
    {
        $objects = $this->normalize($objects);

        foreach ($objects as $object) {
            $this->validate($object);
            $this->_em->remove($object);
        }

        if ($doFlush) {
            $this->_em->flush();
        }
    }

    /**
     * Converts a single Proxy object to an array containing the object.
     *
     * @param object|Collection|LegacyProxy|LegacyProxy[]|Proxy|Proxy[] $objects
     *
     * @return object[]
     */
    private function normalize($objects): array
    {
        if (is_object($objects)) {
            $objects = [$objects];
        }

        return $objects;
    }

    /**
     * Validates if the given object is an entity of the current repository.
     *
     * @param object|LegacyProxy|Proxy $object
     */
    private function validate($object): void
    {
        if (false === is_a($object, $this->getClassName())) {
            $realClass = get_class($object);
            $repositoryClass = get_class($this);
            throw new InvalidArgumentException(
                "Allowed class for saving in $repositoryClass is '{$this->getClassName()}' got $realClass"
            );
        }
    }
}
