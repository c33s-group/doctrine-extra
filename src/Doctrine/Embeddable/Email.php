<?php

declare(strict_types=1);

namespace C33s\Doctrine\Embeddable;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Webmozart\Assert\Assert as MozAssert;

/**
 * @ORM\Embeddable()
 */
class Email
{
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\Email()
     * @Assert\NotBlank()
     */
    protected $address;

    public static function createFromString(string $address): self
    {
        return new self($address);
    }

    /**
     * Email constructor.
     *
     * @param string $address
     */
    public function __construct(?string $address)
    {
        MozAssert::email($address);

        $this->address = $address;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        MozAssert::email($address);
        $this->address = $address;

        return $this;
    }
}
