<?php

declare(strict_types=1);

namespace C33s\Doctrine\Interfaces;

interface ArrayInterface
{
    /**
     * @return self
     */
    public static function fromArray(array $array);

    public function toArray(): array;
}
