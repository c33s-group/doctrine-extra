<?php

declare(strict_types=1);

namespace C33s\Doctrine\Entity\Traits\Behavior;

trait IsPublishable
{
    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $published = false;

    public function isPublished(): bool
    {
        return $this->published;
    }

    public function setPublished(bool $published): self
    {
        $this->published = $published;

        return $this;
    }
}
