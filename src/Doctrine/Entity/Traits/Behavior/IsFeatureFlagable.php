<?php

declare(strict_types=1);

namespace C33s\Doctrine\Entity\Traits\Behavior;

use Flagception\Manager\FeatureManagerInterface;

trait IsFeatureFlagable
{
    /**
     * @see https://packagist.org/packages/flagception/flagception-bundle
     * @see https://packagist.org/packages/flagception/flagception
     * @see https://github.com/bestit/flagception-sdk
     * @see https://github.com/bestit/flagception-bundle
     *
     * @var FeatureManagerInterface
     */
    protected $featureManager;

    public function setFeatureFlagManager(FeatureManagerInterface $featureManager)
    {
        $this->featureManager = $featureManager;
    }
}
