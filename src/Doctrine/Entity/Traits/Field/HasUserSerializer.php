<?php

declare(strict_types=1);

namespace C33s\Doctrine\Entity\Traits\Field;

trait HasUserSerializer
{
    /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize([$this->id, $this->username, $this->password]);
    }

    /**
     * @param mixed $serialized
     *
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->username,
            $this->password) = unserialize($serialized, ['allowed_classes' => ['Ramsey\Uuid\Uuid']]);
    }
}
