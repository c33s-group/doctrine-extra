<?php

declare(strict_types=1);

namespace C33s\Doctrine\Entity\Traits\Field;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

trait HasIsPublished
{
    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=false)
     * @Assert\Type("boolean")
     */
    protected $published = true;

    public function isPublished(): bool
    {
        return $this->published;
    }

    public function setPublished(bool $published): self
    {
        $this->published = $published;

        return $this;
    }
}
