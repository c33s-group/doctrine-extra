<?php

declare(strict_types=1);

namespace C33s\Doctrine\Entity\Traits\Field;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

trait RequiresFirstName
{
    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=30, nullable=false)
     * @Assert\Type("string")
     * @Assert\Length(max=30)
     * @Assert\NotBlank()
     */
    protected $firstName;

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }
}
