<?php

declare(strict_types=1);

namespace C33s\Doctrine\Entity\Traits\Field;

use C33s\Doctrine\Embeddable\Email;
use Doctrine\ORM\Mapping as ORM;

/**
 * Trait HasEmbeddedEmail.
 *
 * Constructor usage in Entity:
 *
 * <code>
 *     use C33s\Doctrine\Entity\Field\HasEmbeddedEmail{
 *          __construct as protected constructHasEmbeddedEmail;
 *     }
 *     public function __construct(?string $email = null, ?string $password = null)
 *     {
 *          $this->constructHasEmbeddedEmail($email);
 *          $this->password = $password;
 *     }
 * </code>
 */
trait HasEmbeddedEmail
{
    /**
     * @var Email
     *
     * @ORM\Embedded(class="C33s\Doctrine\Embeddable\Email")
     */
    protected $email;

    public function __construct(?string $address = null)
    {
        $this->email = new Email($address);
    }

    /**
     * @return Email
     */
    public function getEmail(): ?Email
    {
        return $this->email;
    }

    public function setEmail(Email $email): self
    {
        $this->email = $email;

        return $this;
    }
}
