<?php

declare(strict_types=1);

namespace C33s\Doctrine\Entity\Traits\Field;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

trait HasPostTitle
{
    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=15, nullable=true)
     * @Assert\Type("string")
     * @Assert\Length(max=15)
     */
    protected $postTitle;

    public function getPostTitle(): ?string
    {
        return $this->postTitle;
    }

    public function setPostTitle(?string $title): self
    {
        $this->postTitle = $title;

        return $this;
    }
}
