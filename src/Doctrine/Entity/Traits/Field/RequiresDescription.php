<?php

declare(strict_types=1);

namespace C33s\Doctrine\Entity\Traits\Field;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

trait RequiresDescription
{
    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=false)
     * @Assert\Type("string")
     * @Assert\NotBlank()
     */
    protected $description;

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
