<?php

declare(strict_types=1);

namespace C33s\Doctrine\Entity\Traits\Field;

use C33s\Doctrine\ORM\Id\RespectfulUuidGenerator;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

trait RequiresUuid
{
    /**
     * @var UuidInterface|null
     *
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true, nullable=false)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=RespectfulUuidGenerator::class)
     */
    protected $id;

    public function getId(): ?UuidInterface
    {
        return $this->id;
    }

    public function setId(UuidInterface $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function setIdByString(string $id): self
    {
        $this->id = Uuid::fromString($id);

        return $this;
    }
}
