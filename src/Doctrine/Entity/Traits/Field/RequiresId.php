<?php

declare(strict_types=1);

namespace C33s\Doctrine\Entity\Traits\Field;

use Doctrine\ORM\Mapping as ORM;

trait RequiresId
{
    /**
     * @var int|null
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }
}
