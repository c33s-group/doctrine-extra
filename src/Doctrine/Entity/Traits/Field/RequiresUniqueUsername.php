<?php

declare(strict_types=1);

namespace C33s\Doctrine\Entity\Traits\Field;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

trait RequiresUniqueUsername
{
    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=255, unique=true, nullable=false)
     * @Assert\Length(min="3", max="255")
     */
    protected $username;

    /**
     * @return string
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }
}
