<?php

declare(strict_types=1);

namespace C33s\Doctrine\Entity\Traits\Field;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Trait HasRoles.
 *
 * We use a `array` column in doctrine, so we need to provide arrays but in the backend we only want to allow to use one
 * role per user. the `getRolesForValidator()` creates an array of arrays (with one entry each). in the form we need a
 * mapping to convert from string to array and vice versa.
 *
 * traits cannot define constants, so the constants have to be defined in the entity class where the trait is used (an
 * alternative would be to use static variables).
 *
 * required constants:
 * <code>
 *     const AVAILABLE_ROLES = [
 *         'Editor' => 'ROLE_EDITOR',
 *         'Admin' => 'ROLE_ADMIN',
 *     ];
 *     const DEFAULT_ROLES = [
 *         'Editor' => 'ROLE_EDITOR',
 *     ];
 * </code>
 */
trait HasRoles
{
    /**
     * @var array
     *
     * @ORM\Column(type="simple_array", nullable=true)
     * Assert\NotBlank()
     * @Assert\Choice(callback="getRolesForValidator")
     */
    protected $roles = [];

    /**
     * See class description.
     *
     * @return array
     */
    public static function getRolesForValidator()
    {
        return array_map(
            function ($item) {
                return [$item];
            },
            array_values(self::AVAILABLE_ROLES)
        );
    }

    /**
     * @return array
     */
    public function getRoles(): ?array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }
}
