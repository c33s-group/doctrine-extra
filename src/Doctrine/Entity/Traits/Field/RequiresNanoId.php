<?php

declare(strict_types=1);

namespace C33s\Doctrine\Entity\Traits\Field;

use C33s\Doctrine\ORM\Id\RespectfulNanoIdGenerator;
use C33s\NanoId\NanoId;
use Doctrine\ORM\Mapping as ORM;

trait RequiresNanoId
{
    /**
     * @var NanoId|null
     *
     * @ORM\Id
     * @ORM\Column(type="nanoid", unique=true, nullable=false)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=RespectfulNanoIdGenerator::class)
     */
    protected $id;

    public function getId(): ?NanoId
    {
        return $this->id;
    }

    public function setId(NanoId $id): self
    {
        $this->id = $id;

        return $this;
    }
}
