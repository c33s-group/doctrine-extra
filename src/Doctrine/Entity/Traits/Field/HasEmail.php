<?php

declare(strict_types=1);

namespace C33s\Doctrine\Entity\Traits\Field;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

trait HasEmail
{
    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=50, nullable=true)
     * @Assert\Email()
     * @Assert\Length(max=50)
     */
    protected $email;

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }
}
