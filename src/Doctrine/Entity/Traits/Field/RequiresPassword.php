<?php

declare(strict_types=1);

namespace C33s\Doctrine\Entity\Traits\Field;

use Doctrine\ORM\Mapping as ORM;
use Rollerworks\Component\PasswordStrength\Validator\Constraints as RollerworksPassword;
use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * This trait is mainly used together with c33s/user-bundle.
 */
trait RequiresPassword
{
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\Length(max="255")
     */
    protected $password;

    /**
     * Plain Password to be encoded with event listener.
     *
     * @var string
     *
     * Assert\Length(min="12", max="255")
     * @Assert\NotBlank(groups={"password_plain"})
     * @RollerworksPassword\PasswordStrength(minStrength=4, minLength=0)
     * @RollerworksPassword\PasswordRequirements(
     *     requireLetters=true,
     *     requireCaseDiff=true,
     *     requireNumbers=true,
     *     minLength=12)
     */
    protected $plainPassword;

    /**
     * @var string
     *
     * @SecurityAssert\UserPassword(message="admin.user_edit.wrong_password", groups={"admin_user_profile"})
     */
    protected $oldPassword;

    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * Set Plain Password together with encoder. Password gets encoded using the provided encoder.
     */
    public function setPasswordWithEncoder(callable $encoder, string $plainPassword): self
    {
        $this->password = $encoder($plainPassword);

        return $this;
    }

    /**
     * Encoded Password.
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @param string $plainPassword
     */
    public function setPlainPassword(?string $plainPassword): self
    {
        if (!empty($plainPassword)) {
            $this->plainPassword = $plainPassword;
            // Change some mapped values so preUpdate will get called.
            //            $this->password = ''; // just blank it out // will break the OldPasswordValidator
            $this->setUpdatedAt(new \DateTime());
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    /**
     * @return string
     */
    public function getOldPassword(): ?string
    {
        return $this->oldPassword;
    }

    public function setOldPassword(string $oldPassword): self
    {
        $this->oldPassword = $oldPassword;

        return $this;
    }

    abstract public function setUpdatedAt(\DateTime $date);
}
