<?php

declare(strict_types=1);

namespace C33s\Doctrine\Entity\Traits\Field;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

trait HasFaxNumber
{
    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=36, nullable=true)
     * @Assert\Type("string")
     * @Assert\Length(max=36)
     */
    protected $faxNumber;

    public function getFaxNumber(): ?string
    {
        return $this->faxNumber;
    }

    public function setFaxNumber(?string $faxNumber): self
    {
        $this->faxNumber = $faxNumber;

        return $this;
    }
}
