<?php

declare(strict_types=1);

namespace C33s\Doctrine\Entity\Traits\Method;

trait HasDummySalt
{
    public function getSalt()
    {
        return null;
    }
}
