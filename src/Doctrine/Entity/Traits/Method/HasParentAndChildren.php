<?php

declare(strict_types=1);

namespace C33s\Doctrine\Entity\Traits\Method;

trait HasParentAndChildren
{
    /**
     * @return array<self>
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param self $children
     */
    public function setChildren(array $children): self
    {
        $this->children = $children;

        return $this;
    }

    /**
     * @return self
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param self $parent
     */
    public function setParent($parent): self
    {
        $this->parent = $parent;

        return $this;
    }
}
