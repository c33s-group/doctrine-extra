<?php

declare(strict_types=1);

namespace C33s\Doctrine\Entity\Traits\Method;

// phpcs:disable
@trigger_error('The '.__NAMESPACE__.'\NameInterface interface is deprecated and will be removed in one of the next major releases because symfony deprecated AdvancedUserInterface', E_USER_DEPRECATED);
// phpcs:enable
trait HasDummyAdvancedUserInterface
{
    public function getSalt()
    {
        return null;
    }

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return true;
    }
}
