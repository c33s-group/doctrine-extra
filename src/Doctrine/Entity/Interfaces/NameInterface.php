<?php

declare(strict_types=1);

namespace C33s\Doctrine\Entity\Interfaces;

// phpcs:disable
@trigger_error('The '.__NAMESPACE__.'\NameInterface interface is deprecated and will be removed in one of the next major releases. Use C33s\Doctrine\Entity\Interfaces\Nameable instead.', E_USER_DEPRECATED);
// phpcs:enable

/**
 * @deprecated to be removed in one of the next major versions. Use C33s\Doctrine\Entity\Interfaces\Nameable instead.
 */
interface NameInterface
{
    public function getName(): string;

    /**
     * @return mixed
     */
    public function setName(string $name);
}
