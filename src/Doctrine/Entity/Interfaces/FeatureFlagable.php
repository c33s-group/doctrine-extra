<?php

declare(strict_types=1);

namespace C33s\Doctrine\Entity\Interfaces;

use Flagception\Manager\FeatureManagerInterface;

interface FeatureFlagable
{
    /**
     * @return mixed
     */
    public function setFeatureFlagManager(FeatureManagerInterface $featureManager);
}
