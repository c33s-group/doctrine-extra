<?php

declare(strict_types=1);

namespace C33s\Doctrine\Types;

use C33s\Doctrine\Types\Interfaces\ArrayTypeConversionInterface;
use C33s\Doctrine\Types\Traits\ArrayInterfaceConversionTrait;
use Doctrine\DBAL\Types\JsonType;

abstract class AbstractValueObjectJsonType extends JsonType implements ArrayTypeConversionInterface
{
    use ArrayInterfaceConversionTrait;
}
