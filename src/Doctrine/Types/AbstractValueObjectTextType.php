<?php

declare(strict_types=1);

namespace C33s\Doctrine\Types;

use C33s\Doctrine\Types\Interfaces\StringTypeConversionInterface;
use C33s\Doctrine\Types\Traits\StringInterfaceConversionTrait;
use Doctrine\DBAL\Types\TextType;

/**
 * The same as AbstractValueObjectStringType but extends Doctrines TextType to use the correct database field for text.
 */
abstract class AbstractValueObjectTextType extends TextType implements StringTypeConversionInterface
{
    use StringInterfaceConversionTrait;
}
