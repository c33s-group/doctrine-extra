<?php

declare(strict_types=1);

namespace C33s\Doctrine\Types\Interfaces;

use C33s\Doctrine\Interfaces\StringInterface;
use Doctrine\DBAL\Platforms\AbstractPlatform;

// phpcs:disable Generic.NamingConventions.CamelCapsFunctionName.ScopeNotCamelCaps

interface StringTypeConversionInterface
{
    /**
     * @param mixed $value
     *
     * @return stringInterface
     */
    public function convertToPHPValue($value, AbstractPlatform $platform);

    /**
     * @param mixed $value
     *
     * @return string|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform);
}
