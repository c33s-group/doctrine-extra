<?php

declare(strict_types=1);

namespace C33s\Doctrine\Types\Interfaces;

use C33s\Doctrine\Interfaces\ArrayInterface;
use Doctrine\DBAL\Platforms\AbstractPlatform;

// phpcs:disable Generic.NamingConventions.CamelCapsFunctionName.ScopeNotCamelCaps

interface ArrayTypeConversionInterface
{
    /**
     * @param mixed $value
     *
     * @return arrayInterface
     */
    public function convertToPHPValue($value, AbstractPlatform $platform);

    /**
     * Json encoded array is a string.
     *
     * @param mixed $value
     *
     * @return string|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform);
}
