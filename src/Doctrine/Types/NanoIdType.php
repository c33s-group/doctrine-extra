<?php

declare(strict_types=1);

namespace C33s\Doctrine\Types;

use C33s\NanoId\NanoId;

final class NanoIdType extends AbstractValueObjectStringType
{
    protected $name = 'nanoid';
    protected $valueObjectClass = NanoId::class;
}
