<?php

declare(strict_types=1);

namespace C33s\Doctrine\Types\Traits;

trait NameTrait
{
    /**
     * @var string
     */
    protected $name;

    public function getName(): string
    {
        return $this->name;
    }
}
