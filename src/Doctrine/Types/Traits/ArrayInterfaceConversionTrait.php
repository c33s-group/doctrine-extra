<?php

declare(strict_types=1);

namespace C33s\Doctrine\Types\Traits;

use C33s\Doctrine\Interfaces\ArrayInterface;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use InvalidArgumentException;
use function json_decode;
use Webmozart\Assert\Assert;

// phpcs:disable Generic.CodeAnalysis.UnusedFunctionParameter.FoundAfterLastUsed
// phpcs:disable Generic.NamingConventions.CamelCapsFunctionName.ScopeNotCamelCaps
trait ArrayInterfaceConversionTrait
{
    use NameTrait;
    use TypeHelperTraits;

    /**
     * String which contains the class which implements ArrayInterface.
     *
     * @var string
     */
    protected $valueObjectClass;

    public function convertToPHPValue($value, AbstractPlatform $platform): ?ArrayInterface
    {
        Assert::nullOrString($value);
        $this->validateConfiguredValueObjectClass();
        if (null === $value) {
            return null;
        }
        Assert::true($this->isJson($value));

        /** @var ArrayInterface $class */
        $class = $this->valueObjectClass;

        return $class::fromArray(json_decode($value, true));
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string //json encoded array is a string
    {
        $this->validateConfiguredValueObjectClass();
        if (empty($value)) {
            return null;
        }

        if ($value instanceof ArrayInterface && is_a($value, $this->valueObjectClass, true)) {
            $result = json_encode($value->toArray());

            if (false === $result) {
                throw ConversionException::conversionFailed($this->buildErrorMessage($value), $this->name);
            }

            return $result;
        }

        throw ConversionException::conversionFailed($this->buildErrorMessage($value), $this->name);
    }

    /**
     * Should be called in constructor which is private. just a check to verify that the given $valueObjectClass
     * correctly implements the required Interface.
     */
    private function validateConfiguredValueObjectClass(): void
    {
        if (false === is_a($this->valueObjectClass, ArrayInterface::class, true)) {
            $message =
                'Doctrine Type "'.get_class($this)."'s\" value object class '{$this->valueObjectClass}'".
                ' has to implement '.ArrayInterface::class
            ;
            throw new InvalidArgumentException($message);
        }
    }
}
