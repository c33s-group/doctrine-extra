<?php

declare(strict_types=1);

namespace C33s\Doctrine\Types\Traits;

use C33s\Doctrine\Interfaces\StringInterface;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use InvalidArgumentException;
use Webmozart\Assert\Assert;

trait StringInterfaceConversionTrait
{
    use NameTrait;
    use TypeHelperTraits;

    /**
     * String which contains the class which implements StringInterface.
     *
     * @var string
     */
    protected $valueObjectClass;

    // phpcs:disable Generic.CodeAnalysis.UnusedFunctionParameter.FoundAfterLastUsed
    // phpcs:disable Generic.NamingConventions.CamelCapsFunctionName.ScopeNotCamelCaps
    public function convertToPHPValue($value, AbstractPlatform $platform): ?StringInterface
    {
        Assert::nullOrString($value);
        $this->validateConfiguredValueObjectClass();
        if (empty($value)) {
            return null;
        }

        /** @var StringInterface $class */
        $class = $this->valueObjectClass;

        return $class::fromString($value);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        $this->validateConfiguredValueObjectClass();
        if (null === $value) {
            return null;
        }

        if ($value instanceof StringInterface && is_a($value, $this->valueObjectClass, true)) {
            $string = $value->toString();

            return empty($string) ? null : $string;
        }

        throw ConversionException::conversionFailed($this->buildErrorMessage($value), $this->name);
    }

    /**
     * Should be called in constructor which is private. just a check to verify that the given $valueObjectClass
     * correctly implements the required Interface.
     */
    private function validateConfiguredValueObjectClass(): void
    {
        if (false === is_a($this->valueObjectClass, StringInterface::class, true)) {
            $message =
                'Doctrine Type "'.get_class($this)."'s\" value object class '{$this->valueObjectClass}'".
                ' has to implement '.StringInterface::class
            ;
            throw new InvalidArgumentException($message);
        }
    }
}
