<?php

declare(strict_types=1);

namespace C33s\Doctrine\Listener\Entity;

use C33s\Doctrine\Entity\Interfaces\FeatureFlagable;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Flagception\Manager\FeatureManagerInterface;

class InjectFeatureManagerListener
{
    /**
     * @var FeatureManagerInterface
     */
    protected $featureManager;

    /**
     * @codeCoverageIgnore
     */
    public function __construct(FeatureManagerInterface $featureManager)
    {
        $this->featureManager = $featureManager;
    }

    public function postLoad(LifecycleEventArgs $args): void
    {
        $entity = $args->getEntity();
        if ($entity instanceof FeatureFlagable) {
            $entity->setFeatureFlagManager($this->featureManager);
        }
    }
}
