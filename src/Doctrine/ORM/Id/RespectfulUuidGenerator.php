<?php

declare(strict_types=1);

namespace C33s\Doctrine\ORM\Id;

use Doctrine\ORM\EntityManager;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * Fixes https://github.com/ramsey/uuid-doctrine/issues/81.
 *
 * @see \Ramsey\Uuid\Doctrine\UuidGenerator
 */
final class RespectfulUuidGenerator extends AbstractIdGenerator
{
    /**
     * {@inheritdoc}
     */
    public function generate(EntityManager $em, $entity): ?UuidInterface
    {
        return parent::generate($em, $entity) ?? Uuid::uuid4();
    }
}
